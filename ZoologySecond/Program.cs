﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ZoologySecond
{
    class Program
    {
        static void Main(string[] args)
        {
            var animals = new Dictionary<int, Animal>();

            var animal1 = new Carnivore("South Africa", 8);
            var animal2 = new Herbivore("Spain", 15);
            var animal3 = new Herbivore("Benin", 12);
            var animal4 = new Carnivore("Downtown Oslo", 120);

            animals.Add(animal1.Tag, animal1);
            animals.Add(animal2.Tag, animal2);
            animals.Add(animal3.Tag, animal3);
            animals.Add(animal4.Tag, animal4);

            foreach (var animal in animals)
            {
                Animal a = animal.Value;

                Console.WriteLine($"It's a { a.GetType().Name } of age { a.Age } with the tagnumber { a.Tag } from { a.Location }.");
                a.Breathing();
                a.Eat();
            }

            var duck = new Duck("Akerselva", 2);
            var bear = new BlackBear("Canada", 13);

            duck.Fly();
            duck.Swim();

            bear.Eat();
            bear.Climb();
            bear.Swim();
        }
    }
}
