﻿using System;
using System.Collections.Generic;

namespace ZoologySecond
{
    abstract class Animal
    {
        protected static List<int> TagRegister;

        public string Location { get; set; }
        public int Age { get; set; }
        public int Tag { get; private set; }

        public Animal(string location, int age)
        {
            if (TagRegister == null)
            {
                TagRegister = new List<int>();
            }

            Location = location;
            Age = age;
            Tag = GenerateTag();
        }

        private int GenerateTag()
        {
            Random random = new Random();
            int num = random.Next(1000, 10000);

            if (TagRegister.Contains(num))
            {
                return GenerateTag();
            }
            else
            {
                TagRegister.Add(num);
                return num;
            }
        }

        public virtual void Breathing()
        {
            Console.WriteLine($"Animal { Tag } is breathing. Oh, happy days.");
        }

        public abstract void Eat();
    }

    class Herbivore : Animal
    {
        public Herbivore(string location, int age) : base(location, age)
        {
        }

        public override void Eat()
        {
            Console.WriteLine($"Animal { Tag } eats plants and minds its own buisness.");
        }
    }

    class Carnivore : Animal
    {
        public Carnivore(string location, int age) : base(location, age)
        {
        }

        public override void Eat()
        {
            Console.WriteLine($"Animal { Tag } eats a defenceless animal.");
        }
    }

    interface IFlyer
    {
        public void Fly();
    }

    interface IClimber
    {
        public void Climb();
    }

    interface ISwimmer
    {
        public void Swim();
    }

    class BlackBear : Carnivore, IClimber, ISwimmer
    {
        public BlackBear(string location, int age) : base(location, age)
        {
        }

        public void Climb()
        {
            Console.WriteLine($"The blackbear { Tag } climbs a tree. Poor leafs.");
        }

        public void Swim()
        {
            Console.WriteLine($"The blackbear { Tag } swims in the water. Poor fish.");
        }

        public override void Eat()
        {
            Console.WriteLine($"The blackbear { Tag } eats a defenceless animal.");
        }
    }

    class Duck : Herbivore, IFlyer, ISwimmer
    {
        public Duck(string location, int age) : base(location, age)
        {
        }

        public void Fly()
        {
            Console.WriteLine($"The duck { Tag } flys into the sunset, but sees some bread crumbs and goes down again pretty fast.");
        }

        public void Swim()
        {
            Console.WriteLine($"The duck { Tag } swims with its but sticking up. Is that even swimming?");
        }
    }
}
